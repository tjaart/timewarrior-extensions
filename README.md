# My collection of Timewarrior extensions #

## Installation ##

Copy the scripts in the `extensions` directory to `~/.timewarrior/extensions`

~~~sh
cp extensions/*.py ~/.timewarrior/extensions/
~~~

## daily_totals.py ##

Reports the cumulative time spent per task per day. This is useful if, like me, you have to fill out a time sheet giving the total time spent on a task each day.

The report assumes that the first tag is the task description, and the second tag is the project name. This will work correctly if you use the default `hooks/on-modify.taskwarrior` script.

### Example output ###

~~~sh
$ timew summary :today

Wk  Date       Day Tags                   Start       End    Time   Total
--- ---------- --- ---------------------- ----------- ------ ------ -----
W45 2017-11-10 Fri Task 1, Project1     7:00:00   9:00:00 2:00:00
                   Task 2, Project2     9:00:00  10:00:00 1:00:00
                   Task 1, Project1     10:00:00 11:00:00 1:00:00
                   Task 3, Project3     11:00:00 12:00:00 1:00:00 5:00:00


$ timew daily_totals :today

Date          Time Task   Project
------------- ---- ------ --------
2017-11-10 3:00:00 Task 1 Project1
           1:00:00 Task 2 Project2
           1:00:00 Task 3 Project3
~~~
