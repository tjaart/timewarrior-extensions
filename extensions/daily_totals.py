#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import datetime
import urllib.request

# Copied from the example extension
header = 1
configuration = dict()
body_string = ''
for line in sys.stdin:
    if header:
        if line == '\n':
            header = 0
        else:
            fields = line.strip().split(': ', 2)
            if len(fields) == 2:
                configuration[fields[0]] = fields[1]
            else:
                configuration[fields[0]] = ''
    else:
        body_string += line

# print(configuration)
body = json.loads(body_string)

# max column widths
max_project = 0
max_task = 0

res = {}

for line in body:
    task = line['tags'][0] if 'tags' in line else ''
    project = line['tags'][1] if ('tags' in line) and len(
        line['tags']) > 1 else ''
    if len(task) > max_task:
        max_task = len(task)
    if len(project) > max_project:
        max_project = len(project)

    if not 'end' in line:
        print('daily_totals requires closed interval time periods. Please stop any started tasks and run the script again.')
        exit(2)
    # Hopefully Timewarrior always report time it UTC?
    end = datetime.datetime.strptime(line['end'], '%Y%m%dT%H%M%SZ')
    start = datetime.datetime.strptime(line['start'], '%Y%m%dT%H%M%SZ')
    time = (end - start)
    # set the date in the current timezone
    cur_date = start.replace(
        tzinfo=datetime.timezone.utc).astimezone(tz=None).date()

    if str(cur_date) not in res:
        res[str(cur_date)] = []
        res[str(cur_date)].append(
            {'task': task, 'project': project, 'time': time})
    else:
        day_tasks = list(map(lambda x: x['task'], res[str(cur_date)]))
        if task not in day_tasks:
            res[str(cur_date)].append(
                {'task': task, 'project': project, 'time': time})
        else:
            res[str(cur_date)][day_tasks.index(task)]['time'] += time

print("\033[4m{:10}\033[0m \033[4m{:>8}\033[0m \033[4m{:{max_task}}\033[0m \033[4m{:{max_project}}\033[0m".format(
    'Date', 'Time', 'Task', 'Project', max_task=max_task, max_project=max_project))
for date in res:
    date_str = date
    for row in res[date]:
        # Round the time to nearest 15 minutes
        rounded_time = row['time'] - datetime.timedelta(seconds=row['time'].seconds %
                                                        datetime.timedelta(minutes=15).seconds)

        print("{:10} {:>8} {:{max_task}} {:{max_project}}".format(
            date_str, str(rounded_time), row['task'], row['project'], max_task=max_task, max_project=max_project))
        date_str = ''
